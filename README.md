This is the storage location for mib files to be used in conjunction with the mib manager application. 

The file and folder names for vendor and technology must match the names for vendor and technology in the mib-manager database if you want to add bulk files here and sync using the mib manager db-repo sync