DELTANODE-NOTIFY-MIB  DEFINITIONS ::= BEGIN

    IMPORTS
        MODULE-IDENTITY, OBJECT-TYPE,OBJECT-IDENTITY,
        NOTIFICATION-TYPE, Integer32                 FROM SNMPv2-SMI
        TEXTUAL-CONVENTION                           FROM SNMPv2-TC
        deltanodeNotifies                    FROM DELTANODE-SMI-MIB
        MODULE-COMPLIANCE, OBJECT-GROUP,
        NOTIFICATION-GROUP                           FROM SNMPv2-CONF;

    dnGeneralNotify MODULE-IDENTITY
    LAST-UPDATED "201103011445Z"
    ORGANIZATION "DeltaNode Solutions AB."
    CONTACT-INFO
            "Postal: Kvarnholmsv. 52
                 S-131 31  Nacka
                 Sweden
                 Tel: +46 8 426-1234
                 E-mail: info@deltanode.com "
    
    DESCRIPTION
        "The Structure of Common Notify Information for the
         Delta-Node enterprise."

    REVISION "201103011445Z" -- 3 Mar, 2011
    DESCRIPTION
        "Minor corrections"

    REVISION "200909171607Z" -- 17 Sep, 2009
    DESCRIPTION
        "Flattend stucture due to SNMPc limits.
         Unused dnAgentNotify removed"
    

    REVISION "200905121200Z" -- 12 May, 2009
    DESCRIPTION
        "dn Prefix added."
    
    REVISION "200811131200Z" -- 13 Nov, 2008
    DESCRIPTION
        "New SMI Base."
    
    REVISION "200809120000Z" -- 12 Sep, 2008
    DESCRIPTION
        "NOTIFY remapped to new/ceasing - error,critical, warning tree."

    REVISION "200809100000Z" -- 10 Sep, 2008
    DESCRIPTION
        "The original version at 10 sep"

  ::= {  deltanodeNotifies 1 }

-- ******************************************************************************************
--   Text Conversion
-- ******************************************************************************************
    AgentNotifyLevel ::= TEXTUAL-CONVENTION
        STATUS  current
        DESCRIPTION
            "Notification  leveling."
        SYNTAX  INTEGER {
            information(0),
            warning(1),
            error(2),
            critical(3)
}

    AgentNotifyType ::= TEXTUAL-CONVENTION
        STATUS  current
        DESCRIPTION
            "Notification  Type."
        SYNTAX  INTEGER {
            rising(0),
            ceasing(1)
}

-- ******************************************************************************************
-- Add common trap management v1 and v2
-- ******************************************************************************************

dnBasicEventObjs OBJECT-IDENTITY
    STATUS current
    DESCRIPTION
        "Objects sent by traps"
  ::= { dnGeneralNotify 1 }

-- ******************************************************************************************
--   dnBasicEventObjs - Basic Event Objects
-- ******************************************************************************************

    dnEventObjectType OBJECT-TYPE
        SYNTAX  AgentNotifyType 
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "Indicates the type of trap new(rising) or ceasing. "
  ::= { dnBasicEventObjs 1 }

    dnEventObjectSeverity OBJECT-TYPE
        SYNTAX  AgentNotifyLevel
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "Indicates the level of system severity control. The system has a severity level control
            and each trap should be associated with a severity control. When a trap event occurs
            and its severity is higher than the system severity control level, the trap works
            as defined. If the event severity is lower than the system severity control level,
            the event is ignored as if it did not occur."
  ::= { dnBasicEventObjs 2 }

    dnEventObjectValue OBJECT-TYPE
        SYNTAX  OCTET STRING
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "Current Value sent by trap source with optional unit"
  ::= { dnBasicEventObjs 3 }

    dnEventObjectNode OBJECT-TYPE
        SYNTAX  OCTET STRING
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "Node Name of this Trap"
  ::= { dnBasicEventObjs 4 }

    dnEventObjectRef OBJECT-TYPE
        SYNTAX  Integer32
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "Reference number of this trap"
  ::= { dnBasicEventObjs 5 }

    dnEventObjectCoRef OBJECT-TYPE
        SYNTAX  Integer32
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "Correlated number i.e in case of a ceasing alarm this number identifies the originating trap"
  ::= { dnBasicEventObjs 6 }

    dnEventObjectTimeStamp OBJECT-TYPE
        SYNTAX  Integer32
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "When this trap was generated, UNIX time sec. since 1970 UTC"
  ::= { dnBasicEventObjs 7 }

    dnEventObjectMessage OBJECT-TYPE
        SYNTAX  OCTET STRING
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "Clear text message sent by trap unit"
  ::= { dnBasicEventObjs 8 }

    dnEventObjectClass OBJECT-TYPE
        SYNTAX  Integer32
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "Alarm Class type of Unit etc. DeltaNode CIA
             CIA, Class, Instance and Alarm number, the three numbers forms the actual
             Alarm ID (CIA) which is the Alarm source.
             Class is type of Equipment ex. 256 is the Power Amplifier, 1 is the BGW etc.
             Instance (dnEventObjectUnit) is the actual unit. 
             Alarm-id (dnEventObjectId) is an unique number within the Class."
  ::= { dnBasicEventObjs 9 }

    dnEventObjectUnit OBJECT-TYPE
        SYNTAX  Integer32
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "Instance (or Unit) that is sending this is a part of (CIA) (see dnEventObjectClass)."
  ::= { dnBasicEventObjs 10 }

    dnEventObjectId OBJECT-TYPE
        SYNTAX  Integer32
        MAX-ACCESS  read-only
        STATUS  current
        DESCRIPTION
            "Alarm number, Last part of DeltaNode CIA (see dnEventObjectClass)."
  ::= { dnBasicEventObjs 11 }

-- ******************************************************************************************
--  agentNotifyID
-- ******************************************************************************************

dnDasNotification NOTIFICATION-TYPE
        OBJECTS  { dnEventObjectType,dnEventObjectSeverity,dnEventObjectValue,
                   dnEventObjectNode,dnEventObjectRef,dnEventObjectCoRef,dnEventObjectTimeStamp,
                   dnEventObjectMessage,dnEventObjectClass,dnEventObjectUnit,dnEventObjectId }
        STATUS   current
        DESCRIPTION
                 " If Option 1 is selected this is the only notification that will be sent.
                   All information like Severity etc will be found in the object data."
  ::= { dnGeneralNotify 2 }
--
--  Rising (new) Notifications will be used if Not Option 1 is selected.
--

dnNewCriticalNotify NOTIFICATION-TYPE
        OBJECTS  {
            dnEventObjectValue,dnEventObjectNode,dnEventObjectRef,dnEventObjectCoRef,
            dnEventObjectMessage,dnEventObjectTimeStamp,dnEventObjectClass,
            dnEventObjectUnit,dnEventObjectId
}
        STATUS   current
        DESCRIPTION
                 " This will be sent when a new trap with critical is received
                   No filtering is done"
  ::= { dnGeneralNotify 3 }

dnNewErrorNotify NOTIFICATION-TYPE
        OBJECTS  {
            dnEventObjectValue,dnEventObjectNode,dnEventObjectRef,dnEventObjectCoRef,
            dnEventObjectMessage,dnEventObjectTimeStamp,dnEventObjectClass,
            dnEventObjectUnit,dnEventObjectId
}
        STATUS   current
        DESCRIPTION
                 " This will be sent when a new trap with error level is received
                   No filtering is done"
  ::= { dnGeneralNotify 4 }

dnNewWarningNotify NOTIFICATION-TYPE
        OBJECTS  {
            dnEventObjectValue,dnEventObjectNode,dnEventObjectRef,dnEventObjectCoRef,
            dnEventObjectMessage,dnEventObjectTimeStamp,dnEventObjectClass,
            dnEventObjectUnit,dnEventObjectId
}
        STATUS   current
        DESCRIPTION
                 " This will be sent when a new trap with warning level is received
                   No filtering is done"
  ::= { dnGeneralNotify 5 }

--
-- Ceasing Notifications
--

dnCeasingToErrorNotify NOTIFICATION-TYPE
        OBJECTS  {
            dnEventObjectValue,dnEventObjectNode,dnEventObjectRef,dnEventObjectCoRef,
            dnEventObjectMessage,dnEventObjectTimeStamp,dnEventObjectClass,
            dnEventObjectUnit,dnEventObjectId
}
        STATUS   current
        DESCRIPTION
            " This will be sent when an alarm goes from critical to error.
              If everything works as expected dnEventObjectCoRef will have the originated dnEventObjectRef
              number from the newCriticalNotify"

  ::= { dnGeneralNotify 6 }

dnCeasingToWarningNotify NOTIFICATION-TYPE
        OBJECTS  {
            dnEventObjectValue,dnEventObjectNode,dnEventObjectRef,dnEventObjectCoRef,
            dnEventObjectMessage,dnEventObjectTimeStamp,dnEventObjectClass,
            dnEventObjectUnit,dnEventObjectId
}
        STATUS   current
        DESCRIPTION
           " This will be sent when an alarm goes from critical or error to Warning.
             If everything works as expected dnEventObjectCoRef will have the originated dnEventObjectRef
             number from the newCriticalNotify or newErrorNotify"
  ::= { dnGeneralNotify 7 }

dnCeasingToNoneNotify NOTIFICATION-TYPE
        OBJECTS  {
            dnEventObjectValue,dnEventObjectNode,dnEventObjectRef,dnEventObjectCoRef,
            dnEventObjectMessage,dnEventObjectTimeStamp,dnEventObjectClass,
            dnEventObjectUnit,dnEventObjectId
}
        STATUS   current
        DESCRIPTION
            " This will be sent when an alarm goes from critical, error or Warning to no alarm level.
             If everything works as expected dnEventObjectCoRef will have the originated dnEventObjectRef
             number from the newCriticalNotify, newErrorNotify or newWarningNotify.
             Note that if an alarm goes from newCriticalNotify to ceasingErrorNotify to ceasingWarningNotify
             and finaly to ceasing notify all three ceasing notifies will have the same dnEventObjectRef no."
  ::= { dnGeneralNotify 8 }

-- ==========================================================================================
--   C O M P L I A N C E S
-- ==========================================================================================

notifyCompliances OBJECT IDENTIFIER ::= { dnGeneralNotify 100 }
notifyGroups      OBJECT IDENTIFIER ::= { dnGeneralNotify 101 }

notifyCompliance MODULE-COMPLIANCE
    STATUS current
    DESCRIPTION
        "The requirements for conformance to this MIB. At least
        one of the groups in this module must be implemented to
        conform to the RMON MIB. Implementations of this MIB
        must also implement the system group of MIB-II [16] and the
        IF-MIB [17]."
    MODULE -- this module
    GROUP notificationGroup
    DESCRIPTION
        "This is the notification sent if no other will fit"

    GROUP basicNotifyGroup
    DESCRIPTION
        "This notifications take care of new/ceasing and severity level"

    GROUP dnEventObjGroup
    DESCRIPTION
        "Objects sent with notifications"

  ::= { notifyCompliances 1 }

notificationGroup NOTIFICATION-GROUP
        NOTIFICATIONS {
            dnNewCriticalNotify, dnNewErrorNotify, dnNewWarningNotify,
            dnCeasingToErrorNotify, dnCeasingToWarningNotify,
            dnCeasingToNoneNotify
}
        STATUS current
        DESCRIPTION
            "The BGW Event Group."
  ::= { notifyGroups 1 }

basicNotifyGroup NOTIFICATION-GROUP
        NOTIFICATIONS { dnDasNotification  }
        STATUS current
        DESCRIPTION
            "The BGW Event Group."
  ::= { notifyGroups 2 }

dnEventObjGroup  OBJECT-GROUP
        OBJECTS {
            dnEventObjectType,dnEventObjectSeverity,dnEventObjectValue,
            dnEventObjectNode,dnEventObjectRef,dnEventObjectCoRef,dnEventObjectMessage,
            dnEventObjectTimeStamp,dnEventObjectClass,dnEventObjectUnit,dnEventObjectId
}
        STATUS current
        DESCRIPTION
            "Objects sent in NOTIFICATIONS"
  ::= { notifyGroups 3 }

END
